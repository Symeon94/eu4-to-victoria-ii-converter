package file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import file.eu.ListNode;
import file.eu.Node;
import file.eu.StringNode;
import file.eu.ValueNode;

public class SaveLoader 
{
	public static ListNode loadEU4Save(String path) throws IOException {
		ListNode n = new ListNode("root");
		BufferedReader file = new BufferedReader(new FileReader(new File(path)));
		parseFile(file, n);
		file.close();
		return n;
	}
		
		
	public static void parseFile(BufferedReader file, ListNode currentNode) throws IOException {
		int c = 0;
		StringBuffer buf = new StringBuffer();
		while((c = file.read()) >= 0 && c != '}') {
			if(c <= 32)
				;
			else if(c == '"') {
				String str = readString(file);
				StringNode n = new StringNode(null, str);
				currentNode.addNode(n);
			}
			// On imagine que l'on aura jamais un chiffre tout seul
			/*else if(c == '-' || (c >= '0' && c <= '9')) {
				float val = parseValue(c, file);
				ValueNode n = new ValueNode(null, val);
				currentNode.addNode(n);
			}*/
			else if(c == '{') {
				ListNode tmp = new ListNode(null);
				currentNode.addNode(tmp);
				parseFile(file, tmp);
			}
			else
			{
				buf = new StringBuffer();
				buf.append((char)c);
				int i = readParam(file, buf);
				String p = buf.toString();
				if(i != '=') {
					currentNode.addNode(new StringNode(p, null));
				}
				else {
					// Il peut y avoir un blanc entre le '=' et le suivant charactere
					int ch = skipBlank(file);
					// String node
					if(ch == '"') {
						String str = readString(file);
						currentNode.addNode(new StringNode(p, str));
					}
					// List node
					else if(ch == '{') {
						ListNode tmp = new ListNode(p);
						currentNode.addNode(tmp);
						parseFile(file, tmp);
					}
					// Value Node
					else if(ch == '-' || (ch >= '0' && ch <= '9')) {
						currentNode.addNode(new ValueNode(p, parseValue(ch, file)));
					}
					else if(ch == 'y' || ch == 'Y') {
						currentNode.addNode(new StringNode(p, "yes"));
						file.read();
						file.read();
					}
					else if(ch == 'n' || ch == 'N') {
						currentNode.addNode(new StringNode(p, "no"));
						file.read();
					}
				}
			}
		}
	}
	
	/// Lis jusqu'a atteindre un '='
	private static int readParam(BufferedReader file, StringBuffer buf) throws IOException {
		int i = file.read();
		while(i != '=' && i > 32) {
			buf.append((char)i);
			i = file.read();
		}
		return i;
	}
	
	/// Lis jusqu'a atteindre un '"'
	private static String readString(BufferedReader file) throws IOException {
		int i = file.read();
		StringBuffer buf = new StringBuffer();
		while(i != '"') {
			buf.append((char)i);
			i = file.read();
		}
		return buf.toString();
	}
	
	// Vire les blancs
	private static int skipBlank(BufferedReader file) throws IOException {
		int i = 0;
		while((i = file.read()) <= 32);
		return i;
	}
	
	// Parse un nombre et l'ajoute au noeud actuel
	// TODO integer	
	private static float parseValue(int c, BufferedReader file) throws IOException {
		int i = c;
		boolean neg = false;
		boolean dec = false;
		if(i == '-') {
			neg = true;
			i = file.read();
		}
		float nb = 0;
		float d = 0.1f;
		while(i >= 0) {
			if(i == '.') {
				
			}
			else if(i >= '0' && i <= '9') {
				if(!dec) {
					nb *= 10;
					nb += (i-'0');
				}
				else {
					nb += d*(i-'0');
					d *= 0.1f;
				}
			}
			else if(i == '.')
				dec = true;
			else
				return neg ? -nb : nb;
			i = file.read();
		}
		return 0;
	}
	
	private static float parseValue(BufferedReader file) throws IOException {
		return parseValue(file.read(),file);
	}
}
