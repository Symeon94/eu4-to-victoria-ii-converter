package file.eu;

import java.util.ArrayList;

public class ListNode extends Node
{
	// TODO DICHO
	private ArrayList<Node> subNode;
	
	public ListNode(String param) {
		super(param);
		subNode = new ArrayList<Node>();
	}
	
	public ArrayList<Node> getSubNode() {
		return subNode;
	}

	public int getType() {
		return TYPE_LIST;
	}

	public void addNode(Node n) {
		if(subNode.isEmpty()) {
			subNode.add(n);
			return;
		}
		if(n.getParam() == null) {
			subNode.add(0,n);
			return;
		}
		int max = subNode.size();
		int min = 0;
		int id = (max+min)/2;
		while(max-min > 1) {
			if(subNode.get(id).getParam() == null) 
				min = id;
			else if(subNode.get(id).getParam().compareTo(n.getParam()) > 0) 
				max = id;
			else 
				min = id;
			id = (min+max)/2;
		}
		if(subNode.get(min).getParam().compareTo(n.getParam()) > 0) 
			subNode.add(min, n);
		else
			subNode.add(max, n);
	}
}
