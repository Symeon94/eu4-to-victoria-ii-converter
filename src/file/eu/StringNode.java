package file.eu;

public class StringNode extends Node
{
	private String val;
	
	public StringNode(String param, String val) {
		super(param);
		this.val = val;
	}
	
	public String getString() {
		return val;
	}
	
	public int getType() {
		return TYPE_STRING;
	}
}
