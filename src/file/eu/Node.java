package file.eu;

abstract public class Node 
{
	public static final int TYPE_LIST = 0;
	public static final int TYPE_VALUE = 1;
	public static final int TYPE_STRING = 2;
	
	private String param;
	
	public Node(String param) {
		this.param = param;
	}
	
	abstract public int getType();
	
	public String getParam() {
		return param;
	}
}
