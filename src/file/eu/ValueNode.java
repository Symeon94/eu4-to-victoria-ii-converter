package file.eu;

public class ValueNode extends Node
{
	public static final int TYPE_FLOAT = 0;
	public static final int TYPE_INTEGER = 1;
	
	private int type;
	private int ival;
	private float fval;
	
	public ValueNode(String param, int val) {
		super(param);
		this.ival = val;
		this.fval = 0;
		this.type = TYPE_INTEGER;
	}
	
	public ValueNode(String param, float val) {
		super(param);
		this.fval = val;
		this.ival = 0;
		this.type = TYPE_FLOAT;
	}
	
	public int getValueType() {
		return type;
	}
	
	public int getIntValue() {
		return ival;
	}
	
	public float getFloatValue() {
		return fval;
	}
	
	public int getType() {
		return TYPE_VALUE;
	}
}
