package system;

import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import view.Window;

public class Main 
{
	public static void main(String[] args)
	{
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		catch(Exception e) {e.printStackTrace();}
		try{
			Controller c = new Controller();
			Window w = new Window(c);
			c.setView(w);
		}
		catch(IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "IOException", JOptionPane.ERROR_MESSAGE);
		}
	}
}
