package system;

import java.io.IOException;

import javax.swing.tree.DefaultMutableTreeNode;

import file.SaveLoader;
import file.eu.ListNode;
import file.eu.Node;
import file.eu.StringNode;
import file.eu.ValueNode;
import view.Window;

public class Controller 
{
	private Window w;
	
	public Controller() {
		this.w = null;
	}
	
	public void parseFile(final String path) {
		w.showLoadDialog(true);
		new Thread() {
			public void run() {
				ListNode l = null;
				try {
					l = SaveLoader.loadEU4Save(path);
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				w.setProgressValue(50);
				DefaultMutableTreeNode root = createTree(l);
				w.setProgressValue(100);
				w.setTree(root);	
				w.showLoadDialog(false);
			}
		}.start();
	}

	public void setView(Window w) {
		this.w = w;
	}
	
	private DefaultMutableTreeNode createTree(Node n) {
		DefaultMutableTreeNode res = null;
		String p = n.getParam();
		if(n.getType() == Node.TYPE_LIST) {
			res = new DefaultMutableTreeNode(p == null ? "null" : p);
			for(Node tmp : ((ListNode)n).getSubNode())
				res.add(createTree(tmp));
		}
		else if(n.getType() == Node.TYPE_VALUE) {
			if(p != null) {
				res = new DefaultMutableTreeNode(p);
				res.add(new DefaultMutableTreeNode(((ValueNode)n).getFloatValue()));
			}
			else {
				res = new DefaultMutableTreeNode(((ValueNode)n).getFloatValue());
			}
		}
		else if(n.getType() == Node.TYPE_STRING) {
			if(p != null) {
				res = new DefaultMutableTreeNode(p);
				res.add(new DefaultMutableTreeNode(((StringNode)n).getString()));
			}
			else {
				res =  new DefaultMutableTreeNode(((StringNode)n).getString());
			}
		}
		return res;
	}
}
