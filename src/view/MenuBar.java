package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import system.Controller;

public class MenuBar extends JMenuBar implements ActionListener
{
	private static final long serialVersionUID = -111659266189879540L;

	private JMenu file;
	private JMenuItem file_load;
	private JMenuItem file_export;
	private JMenuItem file_exit;
	
	private JMenu help;
	
	private JFileChooser loadWindow;
	
	private final Controller c;
	
	public MenuBar(final Controller c) {
		super();
		this.c = c;
		loadWindow = new JFileChooser();
		
		file = new JMenu("Fichier");
		file_load = new JMenuItem("Charger");
		file_load.addActionListener(this);
		file_export = new JMenuItem("Exporter");
		file_export.addActionListener(this);
		file_exit = new JMenuItem("Quitter");
		file_exit.addActionListener(this);
		file.add(file_load);
		file.add(file_export);
		file.addSeparator();
		file.add(file_exit);
		
		help = new JMenu("Aide");

		this.add(file);
		this.add(help);
	}

	public void actionPerformed(ActionEvent r) {
		if(r.getSource() == file_load) {
			if(loadWindow.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				c.parseFile(loadWindow.getSelectedFile().getAbsolutePath());
			}
		}
	}
}
