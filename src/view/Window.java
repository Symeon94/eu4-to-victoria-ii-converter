package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


import system.Controller;
import view.dialog.LoadingBarDialog;

public class Window extends JFrame
{
	private static final long serialVersionUID = -3739008754324139579L;

	private JPanel mainPanel;
	private GridBagLayout layout;
	private GridBagConstraints constraint;
	
	private JTree tree;
	private DefaultMutableTreeNode root;
	
	private LoadingBarDialog dialog;
	
	private final Controller c;
	
	public Window(final Controller c) throws IOException {
		super("EU4 to Victoria II Converter");
		this.setIconImage(ImageIO.read(new File("icon.png")));
		this.c = c;
		
		dialog = new LoadingBarDialog(this);
		
		layout = new GridBagLayout();
		constraint = new GridBagConstraints();
		constraint.gridwidth = 1;
		constraint.gridheight = 1;
		constraint.fill = GridBagConstraints.BOTH;
		constraint.gridx = 0;
		
		mainPanel = new JPanel(layout);
		root = new DefaultMutableTreeNode("Sauvegarde");
		tree = new JTree(root);
		
		constraint.weightx = 1;
		constraint.weighty = 0;
		constraint.gridy = 0;
		MenuBar menu = new MenuBar(c);
		mainPanel.add(menu, constraint);
		constraint.weightx = 1;
		constraint.weighty = 1;
		constraint.gridy = 1;
		mainPanel.add(new JScrollPane(tree), constraint);
		
		this.add(mainPanel);
		this.setMinimumSize(new Dimension(400,400));
		this.pack();
		menu.add(Box.createRigidArea(new Dimension(0,menu.getHeight())));
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void setTree(DefaultMutableTreeNode top) {
		root.removeAllChildren();
		root.add(top);
		tree.expandPath(new TreePath(root.getPath()));
	}
	
	public void showLoadDialog(boolean val) {
		dialog.setVisible(val);
		if(!val)
			dialog.setValue(0);
	}

	public void setProgressValue(int i) {
		dialog.setValue(i);
	}
}
