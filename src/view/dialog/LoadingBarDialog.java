package view.dialog;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import view.Window;

public class LoadingBarDialog extends JDialog
{
	private static final long serialVersionUID = -1832298263815412275L;
	
	private JProgressBar b;
	private GridBagLayout l;
	private GridBagConstraints c;
	
	public LoadingBarDialog(final Window parent) {
		super(parent, "Chargement");
		l = new GridBagLayout();
		c = new GridBagConstraints();
		c.insets = new Insets(15,35,15,35);
		c.anchor = GridBagConstraints.CENTER;
		
		b = new JProgressBar();
		b.setPreferredSize(new Dimension(300,25));
		
		this.setUndecorated(true);
		this.setLayout(l);
		c.gridx = 0;
		c.gridy = 0;
		this.add(new JLabel("Chargement"), c);
		c.gridx = 0;
		c.gridy = 1;
		this.add(b, c);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(false);
	}
	
	public void setValue(int val) {
		b.setValue(val);
	}
}
