# README #

Ce git est utilisé pour stocker le convertisseur de sauvegarde pour Europa Universalis 4. L'objectif est d'arriver à créer un programme pour transformer une sauvegarde de Europa Universalis 4 vers Victoria 2.

### Objectifs ###

* Chargement d'une sauvegarde d'Europa Universalis IV
* Creation d'un mod de Victoria II contenant les pays de la Sauvegarde
* Association des régions
* Nations civilisé/non civilisé